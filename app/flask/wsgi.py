from app import app
import os
from os import environ

if __name__ == "__main__":
    if 'ENVIRONMENT' not in environ:
        environ.setdefault('ENVIRONMENT', 'dev')
        print('running in development mode')
    app.run(host='0.0.0.0', port=os.environ.get("FLASK_SERVER_PORT"), debug=True)