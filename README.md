# DevOps Test

## Task 1

The solution is contained in the `app` folder. The actual project is divided into 2 components:

1. the api in the `flask` sub-folder
2. the web server in the `proxy` sub-folder

This is one of the possible configurations that can be used to deploy a Flask web app in a production environment.
The api exposes the following routes:
- `POST /login`: sending valid username and password as parameters the endpoint will return a jwt to use in the auth header
- `GET /comic/random`: the server will return a random comic from the xkcd api as json object
- `GET /metrics`: custom endpoint for healthcheck

To start with a convenience docker compose type `docker-compose up` in the `app` folder; alternatively:

1. set up and activate a virtual environment
2. install dependecies in requirements.txt file
3. `python wsgi.py`

## Task 2

The pipelines implemented belong to the GitLab platfom, because this is what i'm familiar to. In order to build and push a docker image to the internal GitLab container registry the following operations must be done:

1. `git checkout main`
2. `git merge <branch>`
3. `git tag <tag>`
4. `git push --atomic origin main <tag>`

To sum up, only tagged commits will trigger a build pipeline.

To deploy the application on the cluster the push operation must be done on the `production` branch. The triggered job will deploy the app via Helm charts using the `latest` tag. 

The secrets are deployed through Helm charts too, but their deployment is manual, because it can be desirable to have some degree of human intervention on them before deploying. The sensitive values are stored in the GitLab CI/CD variables section and substituted on run time.

![](img/Screenshot%202023-03-16%20at%2007.40.28.png)

## Task 3

I decided to use an existing metric collector, Prometheus (https://prometheus.io/), since it is a well known standard for Kubernetes and it is suited for the task of monitoring a target endpoint and alerting in case of downtime. I installed the client library in the application and then I exposed some metrics on the `/metrics` path; the metric that is relevant to the task is 'Health' which can be connected with an alerting system from prometheus itself or from custom applications.

Prometheus can be deployed manually through the manifests in the `monitoring` folder.

1. create configmap from file `prometheus.yml`
2. deploy service
3. create deployment

![](img/Screenshot%202023-03-16%20at%2007.40.13.png)

## Task 4

I didn't have the possibility to set up a Microsoft Teams channel with a webhook because my development machine is a Macintosh and both the desktop app and the online version for it don't allowed me to use this feature with a personal account. So, I switched to Slack, which is another collaborative tool that is similar to Microsoft Teams.