{{- define "backendImagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": {\"auth\": \"%s\"}}}" .Values.oci.imageCredentials.registry (printf "%s:%s" .Values.oci.imageCredentials.username .Values.oci.imageCredentials.token | b64enc) | b64enc }}
{{- end }}