#!/bin/env sh

# Create config for kubernetes-curl-job.sh script
kubectl create configmap cronjob-script --from-file=script.sh

# Apply Job
kubectl apply -f cronjob.yaml