
from flask import Flask, request, jsonify, session, Response
from datetime import datetime, timedelta
import requests
import random
from flask_jwt_extended import create_access_token
from flask_jwt_extended import jwt_required
from flask_jwt_extended import JWTManager
from os import environ
import prometheus_client
from prometheus_client.core import CollectorRegistry
from prometheus_client import Summary, Counter, Histogram, Gauge, Enum
import psutil
import threading
import time

app = Flask(__name__)

if environ.get('ENVIRONMENT') != 'dev':
    app.config['JWT_SECRET_KEY'] = environ.get('JWT_SECRET_KEY')
    app.secret_key = environ.get('APP_SECRET_KEY')
else:
    app.config['JWT_SECRET_KEY'] = 'default'
    app.secret_key = 'default'
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(seconds=600)
jwt = JWTManager(app)

_INF = float("inf")

graphs = {}
graphs['c'] = Counter('python_request_operations_total', 'The total number of processed requests')# any value that increases (request count, tasks completed, error count)
graphs['h'] = Histogram('python_request_duration_seconds', 'Histogram for the duration in seconds.', buckets=(1, 2, 5, 6, 10, _INF))
graphs['ram_virtual'] = Gauge("memory_usage_bytes_virtual", "Virtual memory usage in bytes.")
graphs['ram_swap'] = Gauge("memory_usage_bytes_swap", "Swap memory usage in bytes.")
graphs['health'] = Enum("app_health", "Health", states=["healthy", "unhealthy"])

cpus = psutil.cpu_count()
i = 1
while(i <= cpus):
    key = 'cpu_' + str(i)
    graphs[key] = Gauge("cpu_" + str(i) +"usage_percent", "CPU usage percent.")
    i +=1

def getPerformances():
    while True:
        time.sleep(1)

        # ram metrics
        ram = psutil.virtual_memory()
        swap = psutil.swap_memory()

        graphs['ram_virtual'].set(ram.used)
        graphs['ram_swap'].set(swap.used)

        # cpu metrics
        idx = 1
        # blocking, per-cpu
        for c, p in enumerate(psutil.cpu_percent(interval=1, percpu=True)):
            key = 'cpu_' + str(idx)
            graphs[key].set(p)
            idx += 1
        
        graphs['health'].state('healthy')

gatherThread = threading.Thread(target=getPerformances)
gatherThread.start()

@app.route('/comic/random')
@jwt_required()
def get_random_comic():
    comic_number = random.randint(0, 2749)
    url = "https://xkcd.com/" + str(comic_number) + "/info.0.json"
    comic = requests.get(url)
    return comic.json()

@app.route('/metrics')
def metrics():
    # return jsonify({'status': 'OK', 'time': datetime.utcnow().isoformat()})
    res = []
    for k,v in graphs.items():
        res.append(prometheus_client.generate_latest(v))
    return Response(res, mimetype="text/plain")

@app.route('/login', methods=['POST'])
def login():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    if username != "test" or password != "test":
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token)

@app.route('/logout', methods=['POST'])
def logout():
    try:
        session['logged_in'] = False
        return jsonify({'status': 'logged out'})
    except:
        return jsonify({'status': 'already logged out'})